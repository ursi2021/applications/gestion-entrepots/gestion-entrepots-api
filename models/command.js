const { DataTypes} = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        senderId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        shippingDate: {
            type: DataTypes.DATE,
            allowNull: false
        },
        dateOfReceipt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        commandType: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isCommandType(value) {
                    if (value !== "Shop" && value !== "Client" && value !== "Supplier") {
                        throw new Error("Command type must be Shop or Client or Supplier.")
                    }
                }
            }
        }
    };

    const options = {
        sequelize, modelName: 'commmand',
        timestamps: false
    };

    return sequelize.define('Command', attributes, options);
}

