const {DataTypes} = require('sequelize')

module.exports = model;

function model(sequelize) {
    const attributes = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        date: {
            type: DataTypes.DATE,
            allowNull: false
        },
        warehouseId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        recipientId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        recipientType: {
            type: DataTypes.STRING,
            validate: {
                isRecipientType(value) {
                    if (value !== "Supplier" && value !== "Client" && value !== "Shop") {
                        throw new Error("A stock position's recipient type must be Supplier or Client or Shop.")
                    }
                }
            },
            allowNull: false
        }
    };

    const options = {
        sequelize, modelName: 'stockMovement',
        timestamps: false
    };

    return sequelize.define('StockMovement', attributes, options);
}
