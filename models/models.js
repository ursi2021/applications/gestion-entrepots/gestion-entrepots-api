const {Sequelize} = require('sequelize');
const stockPositionsJson = require("./samples/stockPositions.json")
let stockPositionsList = stockPositionsJson.stockPositions
const stockMovementsJson = require("./samples/stockMovements.json")
let stockMovementsList = stockMovementsJson.stockMovements
const commandsJson = require('./samples/commands.json')
let commandsList = commandsJson.commands
const logger = require('../logs')
const axios = require("axios");

let models = {};
module.exports = models;

initialize()

async function initialize() {
    const sequelize = new Sequelize(process.env.URSI_DB_NAME, process.env.URSI_DB_USER, process.env.URSI_DB_PASSWORD, {
        host: process.env.URSI_DB_HOST,
        port: Number(process.env.URSI_DB_PORT),
        logging: msg => logger.verbose(msg),
        dialectOptions: {
            timezone: 'Etc/GMT0'
        },
        dialect: 'mariadb'
    });

    // Init models and add them to the exported db object.
    models.StockPosition = require('./stockPosition')(sequelize);
    models.StockPositionEntry = require('./stockPositionEntry')(sequelize);
    models.Command = require('./command')(sequelize);
    models.CommandEntry = require('./commandEntry')(sequelize);
    models.StockMovement = require('./stockMovement')(sequelize);
    models.StockMovementEntry = require('./stockMovementEntry')(sequelize);
    models.ReplenishmentCommand = require('./replenishmentCommand')(sequelize);
    models.replenishmentCommandEntry = require('./replenishmentCommandEntry')(sequelize);
    models.Delivery = require('./delivery')(sequelize);
    models.DeliveryEntry = require('./deliveryEntry')(sequelize);

    // Define associations.
    models.StockPosition.hasMany(models.StockPositionEntry, {as: "stockPositionEntries"});
    models.Command.hasMany(models.CommandEntry, {as: "commandEntries"});
    models.StockMovement.hasMany(models.StockMovementEntry, {as: "stockMovementEntries"});
    models.ReplenishmentCommand.hasMany(models.replenishmentCommandEntry, {as: "replenishmentCommandEntries"})
    models.Delivery.hasMany(models.DeliveryEntry, {as: "deliveryEntries"});

    // Sync all models with database.
    await sequelize.sync();
    logger.info("The tables for gestion-entrepots models were just (re)created!");

    let allStockPositions = await models.StockPosition.findAll()
    if (allStockPositions.length === 0) {
        await initializeStockPosition();
    }
    logger.info("gestion-entrepots app is ready.")
}

async function initializeStockPosition() {

    var nbTries = 0;
    const maxTries = 500;
    let temp = {"message": "Not found"}

    while (nbTries < maxTries) {
        if (nbTries > 0) {
            setTimeout(() => {
                logger.debug("Waiting 0.5 sec for referentiel-produit...")
            }, 500)
        }
        logger.debug("Testing referentiel produit is registered to kong : " + nbTries + " tries")
        try {
            temp = await axios.get("http://kong:8081/services/referentiel-produit")
            break
        } catch (e) {
            nbTries += 1;
        }
    }

    if (nbTries >= maxTries) {
        logger.error("referentiel produit seems unregistered to kong after " + nbTries + "checks. \nExiting...")
        for (const key in temp) {
            console.log(key + " " + temp[key])
        }
        process.exit(1)
    }

    logger.debug("Getting products from referentiel-produit to set initial stock.")

    let res = null
    nbTries = 0
    const nbTriesMax = 20;

    while (res === null && nbTries < nbTriesMax) {

        if (nbTries > 0) {
            setTimeout(() => {
                logger.debug("Waiting 0.5 sec for referentiel-produit...")
            }, 500);
        }

        logger.debug("Tryng to connect to referentiel produit ( " + nbTries + " tries)")
        try {
            res = await axios.get(process.env.URL_REFERENTIEL_PRODUIT + "/products");
        } catch (e) {
        }
        nbTries += 1;
    }

    if (res === null) {
        logger.error("Could not connect to referentiel produit after " + nbTries + " tries.\n Exiting.")
        process.exit(1)
    } else {
        logger.debug("Sucessfully connected to referentiel produit after " + nbTries + " tries.")
    }

    let stockPosition = await models.StockPosition.create()

    for (let index in res.data) {
        await models.StockPositionEntry.create({
            productCode: res.data[index]["product-code"],
            quantity: res.data[index]["min-quantity"],
            StockPositionId: stockPosition.id
        })
    }
}

async function setUpDummyData() {

    logger.debug("Preparing to set up dummy data")
    await setupDummyCommands();
    await setupDummyStockMovements();
    await setUpDummyReplenishmentCommand();
}

async function setupDummyStockPositions() {

    let stockPositions = await models.StockPosition.findAll();
    let nbStockPositions = stockPositions.length;

    if (nbStockPositions === 0) {

        logger.debug("stockPositions dummy data");
        for (let i = 0; i < stockPositionsList.length; i++) {

            let currentStockPosition = stockPositionsList[i];
            let date = new Date(currentStockPosition.date)
            console.log(date)
            await models.StockPosition.create({

                id: currentStockPosition["warehouse-id"],
                date: date
            });

            // Using the "products" field of the json, we generate all needed stockPositionEntries
            let lastStockPos = await models.StockPosition.findOne({
                include: ["stockPositionEntries"],
                order: [['date', 'DESC']],
            });
            for (let j = 0; j < currentStockPosition.products.length; j++) {

                let currentProductEntry = currentStockPosition.products[j];
                let productCode = lastStockPos.stockPositionEntries[0].productCode;
                let quantity = currentProductEntry.quantity
                await models.StockPositionEntry.create({
                    productCode: productCode,
                    quantity: quantity,
                    StockPositionId: currentStockPosition["warehouse-id"]
                });
            }
        }
    }
}

async function setupDummyCommands() {

    let commands = await models.Command.findAll();
    let nbCommands = commands.length;

    if (nbCommands === 0) {

        logger.debug("stockPositions dummy data")
        for (let i = 0; i < commandsList.length; i++) {
            let currentCommand = commandsList[i]
            let dateShipping = new Date(currentCommand["shipping-date"])
            let dateReceipt = new Date(currentCommand["date-of-receipt"])
            await models.Command.create({
                id: currentCommand.id,
                senderId: currentCommand["sender-id"],
                shippingDate: currentCommand["shipping-date"],
                dateOfReceipt: currentCommand["date-of-receipt"],
                commandType: currentCommand["command-type"]
            });

            // Using the "products" field of the json, we generate all needed commandEntries
            let lastStockPos = await models.StockPosition.findOne({
                include: ["stockPositionEntries"],
                order: [['date', 'DESC']],
            });
            for (let j = 0; j < currentCommand.products.length; j++) {

                let currentCommandEntry = currentCommand.products[j];
                let productCode = lastStockPos.stockPositionEntries[0].productCode;
                let quantity = currentCommandEntry.quantity
                await models.CommandEntry.create({
                    productCode: productCode,
                    quantity: quantity,
                    CommandId: currentCommand.id
                });
            }
        }
    }
}

async function setupDummyStockMovements() {

    let stockMovement = await models.StockMovement.findAll();
    let nbStockMovements = stockMovement.length;

    if (nbStockMovements === 0) {

        logger.debug("stockMovement dummy data");
        for (let i = 0; i < stockMovementsList.length; i++) {

            let currentStockMovement = stockMovementsList[i];
            await models.StockMovement.create({

                id: currentStockMovement.id,
                date: currentStockMovement.date,
                warehouseId: currentStockMovement["warehouse-id"],
                recipientId: currentStockMovement["recipient-id"],
                recipientType: currentStockMovement["recipient-type"]
            });
            let lastStockPos = await models.StockPosition.findOne({
                include: ["stockPositionEntries"],
                order: [['date', 'DESC']],
            });

            // Using the "products" field of the json, we generate all needed stockMovementEntries
            for (let j = 0; j < currentStockMovement.products.length; j++) {

                let currentStockMovementEntry = currentStockMovement.products[j];
                let productCode = lastStockPos.stockPositionEntries[0].productCode;
                let quantity = currentStockMovementEntry.quantity
                await models.StockMovementEntry.create({
                    productCode: productCode,
                    quantity: quantity,
                    StockMovementId: currentStockMovement.id
                });
            }
        }
    }
}

async function setUpDummyReplenishmentCommand() {
    // TODO initialize from json

    let replenishmentCommands = await models.ReplenishmentCommand.findAll();
    let nbReplenishmentCommands = replenishmentCommands.length;
    logger.info("There are " + nbReplenishmentCommands + " replenishmentCommands in the db.")

    if (nbReplenishmentCommands === 0) {
        let now = new Date()
        await models.ReplenishmentCommand.create({id: 1, warehouseId: 1, date: now})
        let lastStockPos = await models.StockPosition.findOne({
            include: ["stockPositionEntries"],
            order: [['date', 'DESC']],
        });
        await models.replenishmentCommandEntry.create({
            id: 1,
            productCode: lastStockPos.stockPositionEntries[0].productCode,
            quantity: "42",
            ReplenishmentCommandId: 1
        })
    }
}

async function setUpDummyDelivery() {

    let deliveries = await models.Delivery.findAll();
    let nbDeliveries = deliveries.length;

    if (nbDeliveries === 0) {

        let now = new Date();

        await models.Delivery.create({
            id: 1,
            warehouseId: 1,
            recipientId: 1,
            shippingDate: now,
            dateOfReceipt: null,
            type: "Shop",
            state: "Shipped"
        })

        await models.Delivery.create({
            id: 2,
            warehouseId: 1,
            shippingDate: new Date("02/02/2020 10:50:00"),
            dateOfReceipt: now,
            type: "Shop",
            state: "Delivered"
        });

        await models.DeliveryEntry.create({id: 3, productCode: "4047443190659", quantity: 42, DeliveryId: 1})
        await models.DeliveryEntry.create({id: 1, productCode: "4047443105936", quantity: 10, DeliveryId: 2})
        await models.DeliveryEntry.create({id: 2, productCode: "4047443190659", quantity: 5, DeliveryId: 2})
    }
}
