const {DataTypes} = require('sequelize')

module.exports = model;

function model(sequelize) {
    const attributes = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        productCode: {
            type: DataTypes.STRING,
            allowNull: false
        },
        quantity: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    };

    const options = {
        sequelize, modelName: 'replenishmentCommandEntry',
        timestamps: false
    };

    return sequelize.define('ReplenishmentCommandEntry', attributes, options);
}
