const models = require("../models/models");
const {DataTypes} = require('sequelize')

module.exports = model;

function model(sequelize) {
    const attributes = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        date: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date()
        }
    };

    const options = {
        sequelize, modelName: 'stockPosition',
        timestamps: false
    };

    return sequelize.define('StockPosition', attributes, options);
}


