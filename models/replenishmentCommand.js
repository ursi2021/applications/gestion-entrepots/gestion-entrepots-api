const {DataTypes} = require('sequelize')

module.exports = model;

function model(sequelize) {
    const attributes = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        warehouseId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        date: {
            type: DataTypes.DATE,
            allowNull: false
        }
    };

    const options = {
        sequelize, modelName: 'replenishmentCommand',
        timestamps: false
    };

    return sequelize.define('ReplenishmentCommand', attributes, options);
}
