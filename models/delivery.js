const { DataTypes} = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        warehouseId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        recipientId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        shippingDate: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date()
        },
        dateOfReceipt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        type: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isDeliveryType(value) {
                    if (value !== "Shop" && value !== "Client" ) {
                        throw new Error("Delivery type must be Shop or Client.")
                    }
                }
            }
        },
        state: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isState(value) {
                    if (value !== "Shipped" && value !== "Delivered" && value !== "Cancelled") {
                        throw new Error("Delivery state must be Shop or Client.")
                    }
                }
            }
        },
        orderId: {
            type: DataTypes.INTEGER,
            allowNull: true
        }
    };

    const options = {
        sequelize, modelName: 'delivery',
        timestamps: false
    };

    return sequelize.define('Delivery', attributes, options);
}

