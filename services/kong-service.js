var axios = require('axios');
const logger = require("../logs");

function register() {
    let appName = process.env.APP_NAME || "gestion-entrepots"
    axios
        .post('http://kong:8081/services/', {
            name: appName,
            url: 'http://' + appName
        })
        .then(res => {
            console.log(`statusCode: ${res.statusCode}`)
            console.log(res)
        })
        .catch(error => {
            console.error(error)
            return false;
        })

    axios
        .post('http://kong:8081/services/' + appName + '/routes', {
            paths: ["/" + appName],
            name: appName
        })
        .then(res => {
            console.log(`statusCode: ${res.statusCode}`)
            console.log(res)
        })
        .catch(error => {
            console.error(error)
            return false;
        })
    return true;
}


function registerToKong() {
        let registered = false;
        while (registered === false) {
            registered = register();
            logger.debug("Registering gestion entrepots in Kong...")
        }

        logger.debug("Gestion entrepots is registered in Kong.")

}

module.exports.registerToKong = registerToKong
