var express = require('express');
var router = express.Router();
var app = express();
var logger = require('../logs');
var axios = require("axios");
const dotenv = require("dotenv");
const models = require("../models/models");
const StockPositionController = require('../controller/stockPositionController')
const PagesController = require('../controller/pagesController')
const TestsController = require('../controller/testsController')
const ReplenishmentCommandController = require('../controller/replenishmentCommandController')
const DeliveryController = require('../controller/deliveryController')

var deliveryList = deliveryList = {
  "commands": [
    {
      "id": 1,
      "sender-id": 1,
      "shipping-date": "2020-10-23 23:10:00",
      "date-of-receipt": "2020-10-23 23:10:00",
      "command-type": "Shop",
      "products": [{
        "product-code": "BLABLA",
        "quantity": 42
      }]
    }
  ]
};

dotenv.config()

var template = require('../controller/template_controller')(app);


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/', template.index)

/**@swagger
 * /users:
 *   get:
 *     tags:
 *       - Products
 *     summary: get users page
 *     responses:
 *       200:
 *         description: Return users html
 */
router.get('/users', function (req, res, next) {
    //
    User.findAll().then(function (users) {
        logger.info(users);
        res.render('users', {title: 'Users', users: users});
    });
});


/**@swagger
 * /gestion-entrepots/hello:
 *   get:
 *     tags:
 *     summary: get hello from gestion entrepot app
 *     responses:
 *       200:
 *         description: Return json with key "gestion-entrepots" and value "Hello World !"
 */
router.get('/gestion-entrepots/hello', TestsController.hello)


/**@swagger
 * /gestion-entrepots/hello/all:
 *   get:
 *     tags:
 *     summary: get hello from all apps
 *     responses:
 *       200:
 *         description: Return web page with all hello
 *       404:
 *         description : If an error occurred
 */
router.get("/gestion-entrepots/hello/all", TestsController.renderHelloAll);


/**@swagger
 * /gestion-entrepots/products:
 *   get:
 *     tags:
 *     summary: get products from referentiel-produit app
 *     responses:
 *       200:
 *         description: Return web page with all products
 */
router.get('/gestion-entrepots/products', TestsController.renderProducts)


/**@swagger
 * TODO
 */
router.get('/gestion-entrepots/clients', TestsController.renderClients)


/** @swagger
 * TODO
 */
router.get('/gestion-entrepots/warehouse-stock-position', StockPositionController.getStockPosition)



/** @swagger
 * TODO
 */
router.get('/gestion-entrepots/page1', PagesController.renderPage1)


/** @swagger
 * TODO
 */
router.get('/gestion-entrepots/reapprovisionnement', ReplenishmentCommandController.sendReplenishmentCommand)

router.get('/gestion-entrepots/page2', PagesController.renderPage2)

router.get("/gestion-entrepots/delivery-shop", DeliveryController.getShopDeliveries)

router.get("/gestion-entrepots/delivery-shop/:shopId", DeliveryController.getShopDeliveries)


router.get("/gestion-entrepots/delivery-client", DeliveryController.getAllClientDeliveries)

router.get("/gestion-entrepots/delivery-client/:account", DeliveryController.getOneClientDeliveries)

router.post("/gestion-entrepots/data/web-sales", DeliveryController.storeWebSale)

module.exports = router;