let controller = {};
const logger = require("../logs");
let models = require('../models/models');

module.exports = controller

controller.sendReplenishmentCommand = async function (req, res) {
    try {

        let replenishmentCommands = await models.ReplenishmentCommand.findAll({include: ["replenishmentCommandEntries"]});
        let result = []

        // Transform the object into a json fitting the spec.
        for (let i = 0; i < replenishmentCommands.length; i++) {

            let productQuantityMap = replenishmentCommands[i]["replenishmentCommandEntries"].map(entry => {
                return {
                    "product-code": entry.productCode,
                    quantity: entry.quantity
                }
            })
            let date = replenishmentCommands[i].date
            let dateString = "" + date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + " " +
                date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds()

            result.push({
                id: replenishmentCommands[i].id,
                "warehouse-id": replenishmentCommands[i].warehouseId,
                date: dateString,
                "product-quantity-map": productQuantityMap
            })
        }

        res.status(200).send(result)
    } catch (e) {

        logger.error("Error while getting the replenishment commands : " + e.toString());
        res.status(500);
    }
}