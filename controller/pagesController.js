let controller = {};
let models = require('../models/models')
const logger = require("../logs");
const axios = require('axios')
const deliveryController = require('./deliveryController')

module.exports = controller

controller.renderPage1 = async function (req, res) {

    let orders = await getOrdersFromGestComm(res)
    let sales = await deliveryController.fetchAllClientsDeliveries()
    let deliveryList = await getDeliveryRequestsFromGestComm(res)
    res.render('page1', {deliveryList: deliveryList, sales: sales, supplierOrderList: orders});
}


/**
 *
 * @param res the http response object, to be able to send errors.
 * @returns {Promise<*>}
 */
async function getOrdersFromGestComm(res) {
    try {
        let orders = await axios.get(process.env.URL_GESTION_COMMERCIALE + "/provider-orders");

        for (let i = 0; i < orders.data.length; i++) {
            let stockMovement = await models.StockMovement.build({
                date: new Date(),
                warehouseId: 1,
                recipientId: orders.data[i]["provider-id"],
                recipientType: "Supplier",
            });
            await stockMovement.save();

            for (let j = 0; j < orders.data[i]["product-quantity-map"].length; j++) {
                await models.StockMovementEntry.create({
                    productCode: orders.data[i]["product-quantity-map"][j]["product-code"],
                    quantity: orders.data[i]["product-quantity-map"][j].quantity,
                    StockMovementId: stockMovement.id
                });
            }
        }

        return orders.data;

    } catch (e) {

        logger.error("Couldn't get gestion commerciale provider orders : " + e.toString());
        res.status(404);
    }
}


/**
 *
 * @param res the http response object, to be able to send errors.
 * @returns {Promise<*>}
 */
async function getDeliveryRequestsFromGestComm(res) {
    try {
        let deliveryList = await axios.get(process.env.URL_GESTION_COMMERCIALE + "/delivery-requests");
        for (let i = 0; i < deliveryList.data.length; i++) {
            let stockMovement = await models.StockMovement.create({
                date: new Date(),
                warehouseId: 1,
                recipientId: deliveryList.data[i]["destination-id"],
                recipientType: "Shop",
            });

            let delivery = await models.Delivery.create({
                warehouseId: 1,
                recipientId : deliveryList.data[i]["destination-id"],
                type: "Shop",
                state: "Shipped"
            })

            for (let j = 0; j < deliveryList.data[i]["product-quantity-map"].length; j++) {
                await models.StockMovementEntry.create({
                    productCode: deliveryList.data[i]["product-quantity-map"][j]["product-code"],
                    quantity: deliveryList.data[i]["product-quantity-map"][j].quantity,
                    StockMovementId: stockMovement.id
                });


                await models.DeliveryEntry.create({
                    productCode: deliveryList.data[i]["product-quantity-map"][j]["product-code"],
                    quantity: deliveryList.data[i]["product-quantity-map"][j].quantity,
                    DeliveryId: delivery.id
                })
            }
        }

        return deliveryList.data;

    } catch (e) {

        logger.error("Couldn't get gestion commerciale delivery requests : " + e.toString());
        res.status(404);
    }
}

controller.renderPage2 = async function (req, res) {
    try {
        const stockPosition = await renewStockPosition()
        res.render('page2', {positionStock: stockPosition, positionStockEntries: stockPosition.stockPositionEntries});
    } catch (e) {
        logger.error("Couldn't get any Stock : " + e.toString());
        res.status(404);
    }
}
