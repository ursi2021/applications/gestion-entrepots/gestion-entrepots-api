let controller = {};
const models = require('../models/models')
const logger = require("../logs");
const {Op} = require('sequelize')

module.exports = controller


/**
 * Renew the stock position from recent stock movements then send it back.
 * If no movements were made recently, the last stock position is returned.
 *
 * @param req
 * @param res
 * @returns The current stock position, as json.
 */
controller.getStockPosition = async function (req, res) {

    stockPosition = await renewStockPosition()
    res.setHeader('Content-Type', 'application/json');

    // Transform the object into a json fitting the spec.
    let productQuantityMap = stockPosition["stockPositionEntries"].map(entry => {
        return {
            "product-code": entry.productCode,
            quantity: entry.quantity
        }
    })

    let result = {
        id: stockPosition.id,
        date: stockPosition.date,
        "product-quantity-map": productQuantityMap
    }
    res.json(result);
}


renewStockPosition = async function() {
    try {
        let lastStockPos = await models.StockPosition.findOne({
            include: ["stockPositionEntries"],
            order: [['date', 'DESC']],
        });
        const movements = await models.StockMovement.findAll({ include: ["stockMovementEntries"], where: { date: {[Op.gte]: lastStockPos.date}}})
        if (movements.length === 0){
            return lastStockPos
        }
        let newStockPosition = await models.StockPosition.create({date: new Date()})
        await applyMovement(lastStockPos, newStockPosition.id, movements);
        let result = await models.StockPosition.findOne({
            include: ["stockPositionEntries"],
            where: {id: newStockPosition.id}
        });
        return result
    }
    catch (e) {
        logger.error("error in renewStockPosition: " + e)
    }
}


async function applyMovement(lastPos, newStockPositionId, movements){

    for (let j in movements) {
        let inOrOut = -1
        if (movements[j].recipientType === "Supplier"){
            inOrOut = 1
        }
        for (let i in movements[j].stockMovementEntries) {
            if (lastPos.stockPositionEntries.find(element => element.productCode === movements[j].stockMovementEntries[i].productCode) !== undefined) {
                lastPos.stockPositionEntries[i].quantity += movements[j].stockMovementEntries[i].quantity * inOrOut
            } else {
                await models.StockPositionEntry.create({
                    StockPositionId: newStockPositionId,
                    productCode: movements[j].stockMovementEntries[i].productCode,
                    quantity: movements[j].stockMovementEntries[i].quantity * inOrOut
                })
            }
        }
    }
    for (let k in lastPos.stockPositionEntries){
        await models.StockPositionEntry.create({
            StockPositionId: newStockPositionId,
            productCode: lastPos.stockPositionEntries[k].productCode,
            quantity: lastPos.stockPositionEntries[k].quantity
        })
    }
}