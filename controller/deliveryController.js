let controller = {};
let models = require('../models/models')
const logger = require("../logs");

module.exports = controller

controller.getShopDeliveries = async function (req, res) {

    try {
        let shopDeliveries;

        if (!req.params.shopId) {
            shopDeliveries = await models.Delivery.findAll({
                where: {type: 'Shop'},
                include: "deliveryEntries"
            })
        } else {
            shopDeliveries = await models.Delivery.findAll({
                where: {type: 'Shop', recipientId: req.params.shopId},
                include: "deliveryEntries"
            })
        }


        let result = []

        // Transform the object into a json fitting the spec.
        for (let i = 0; i < shopDeliveries.length; i++) {

            let productQuantityMap = shopDeliveries[i]["deliveryEntries"].map(entry => {
                return {
                    "product-code": entry.productCode,
                    quantity: entry.quantity
                }
            })

            result.push({
                id: shopDeliveries[i].id,
                "warehouse-id": shopDeliveries[i].warehouseId,
                shippingDate: shopDeliveries[i].shippingDate,
                dateOfReceipt: shopDeliveries[i].dateOfReceipt,
                "product-quantity-map": productQuantityMap
            })
        }

        res.json(result)
    } catch (e) {

        logger.error("Error while getting the shops deliveries : " + e.toString());
        res.status(500);
    }
}

controller.getOneClientDeliveries = async function (req, res) {

    try {
        let clientDeliveries = null
        clientDeliveries = await models.Delivery.findAll({
            where: {
                type: 'Client',
                recipientId: req.params.account
            }, include: "deliveryEntries", group: ["orderId"]
        })
        let orderList = []

        // Transform the object into a json fitting the spec.
        for (let i = 0; i < clientDeliveries.length; i++) {

            let orders = {
                "order-id": clientDeliveries[i].orderId,
                "shipping-date": clientDeliveries[i].shippingDate,
                "date-of-receipt": clientDeliveries[i].dateOfReceipt,
                "state-order": clientDeliveries[i].state
            }

            orderList.push(orders)
        }


        res.json({"order-list": orderList})
    } catch (e) {

        logger.error("Error while getting the shops deliveries : " + e.toString());
        res.status(500);
    }
}

controller.fetchAllClientsDeliveries = async function() {
    try {
        let clientDeliveries = await models.Delivery.findAll({
            where: {type: "Client"},
            include: "deliveryEntries",
            group: ["recipientId", "orderId"]
        })

        let result = []

        // Transform the object into a json fitting the spec.
        for (let i = 0; i < clientDeliveries.length; i++) {

            let orderList = clientDeliveries[i]["deliveryEntries"].map(entry => {
                return {
                    "order-id": clientDeliveries[i].orderId,
                    "shipping-date": clientDeliveries[i].shippingDate,
                    "date-of-receipt": clientDeliveries[i].dateOfReceipt,
                    "state-order": clientDeliveries[i].state
                }
            })

            result.push({account: clientDeliveries[i].recipientId, "order-list": orderList})
        }
        return result

    } catch (e) {

        logger.error("Error while getting the shops deliveries : " + e.toString());
        return null;
    }
}

controller.getAllClientDeliveries = async function (req, res) {

    try {
        let result = await controller.fetchAllClientsDeliveries();

        if (result == null) {
            res.status(500).send()
            return
        }

        res.json(result)
    } catch (e) {

        logger.error("Error while getting the shops deliveries : " + e.toString());
        res.status(500);
    }
}

controller.storeWebSale = async function (req, res) {
    try {
        let sale = req.data

        // Prepare removal of products from stock
        let stockMovement = await models.StockMovement.create({
            date: new Date(),
            warehouseId: 1,
            recipientId: sale.data[i]["account"] + "",
            recipientType: "Client",
        });

        // Track delivery status of the order
        let delivery = await models.Delivery.create({
            warehouseId: 1,
            orderId: sale.data[i].id,
            recipientId: sale.data[i]["account"],
            type: "Client",
            state: "Shipped"
        });

        for (let j = 0; j < sale.data[i]["product-quantity-map"].length; j++) {

            // TODO check if there is enough stock (no negatives)

            // Remove product from stock
            await models.StockMovementEntry.create({
                productCode: sale.data[i]["product-quantity-map"][j]["product-code"],
                quantity: sale.data[i]["product-quantity-map"][j].quantity,
                StockMovementId: stockMovement.id
            });

            // Create deliveryEntry
            await models.DeliveryEntry.create({
                productCode: sale.data[i]["product-quantity-map"][j]["product-code"],
                quantity: sale.data[i]["product-quantity-map"][j].quantity,
                DeliveryId: delivery.id
            })
        }

    } catch (e) {

        logger.error("Error while receiving webSale via POST : " + e);
        res.status(500).send("Internal Error");
    }
}
