let controller = {};
const axios = require("axios");
const logger = require("../logs");

module.exports = controller

controller.renderClients = function (req, res) {
    try {

        axios.get(process.env.URL_RELATION_CLIENT + "/clients").then((response) => {

                if (response.data) {
                    res.render('clients', {clients: response.data});
                }
            }
        );

    } catch (e) {

        logger.error("Couldn't fetch clients information : " + e.toString());
        res.status(404);
    }
}


controller.renderProducts = function (req, res) {
    try {

        axios.get(process.env.URL_REFERENTIEL_PRODUIT + "/produits").then((response) => {

                if (response.data) {
                    res.render('products', {products: response.data});
                }
            }
        );

    } catch (e) {

        logger.error("Couldn't fetch products information : " + e.toString());
        res.status(404);
    }
}

controller.hello = function (req, res, next) {

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({'gestion-entrepots': 'Hello World !'}));
}


controller.renderHelloAll = async function (req, res) {
    try {

        const urls = [
            {name: "Relation client", url: process.env.URL_RELATION_CLIENT},
            {name: "Decisionnel", url: process.env.URL_DECISIONNEL},
            {name: "Gestion promotion", url: process.env.URL_GESTION_PROMOTION},
            {name: "Referenciel produit ", url: process.env.URL_REFERENTIEL_PRODUIT},
            {name: "Caisse", url: process.env.URL_CAISSE},
            {name: "Monetique paiement", url: process.env.URL_MONETIQUE_PAIEMENT},
            {name: "Back Office", url: process.env.URL_BACK_OFFICE_MAGASIN},
            {name: "E Commerce", url: process.env.URL_E_COMMERCE},
            {name: "Entrepots", url: process.env.URL_ENTREPROTS},
        ];
        const ret = await Promise.all(
            urls.map(async (url) => {
                try {
                    const response = await axios.get(url.url + "/hello");
                    const ret = {...url, data: JSON.stringify(response.data), error: false};
                    return ret;
                } catch (err) {
                    return {...url, data: null, error: err};
                }
            })
        );
        res.render("hello_all", {apps: ret});

    } catch (e) {
        logger.error("error in hello all : "  + e);
        res.sendStatus(404);
    }
}

